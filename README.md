Session3: Communications using topics (https://sir.upc.edu/projects/rostutorials)

Code a new node called pubvelsafe (in a new file pubvelsafe.cpp) in order to send:
1. A random angular velocity command and a fixed linear velocity of 1.0 when the turtle is placed in a safe zone defined by a square around the center of the window.
2. A random angular velocity and a random linear velocity (i.e. as done in pubvel.cpp) otherwise.
