
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>  // For geometry_msgs::Twist
#include <stdlib.h>               // For rand() and RAND_MAX
#include <turtlesim/Pose.h>
#include <iomanip>  // for std::setprecision and std::fixed

ros::Publisher pub;

void monitoringTurtle(const turtlesim::Pose& msg)
{
    ROS_INFO_STREAM(std::setprecision(2) << std::fixed << "position=(" << msg.x << "," << msg.y << ")"
                                         << " direction=" << msg.theta);
    geometry_msgs::Twist move;

    // square of side 5 cm
    if (msg.x > 3 && msg.x < 8 && msg.y > 3 && msg.y < 8) {
        move.linear.x = 1.0;
        move.angular.z = 2 * double(rand()) / double(RAND_MAX) - 1;
    } else {
        move.linear.x = double(rand()) / double(RAND_MAX);
        move.angular.z = 2 * double(rand()) / double(RAND_MAX) - 1;
    }
    pub.publish(move);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "pubvelsafe");
    ros::NodeHandle nh;

    // Seed the random number generator.
    srand(time(0));

    pub = nh.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1000);

    ros::Subscriber sub = nh.subscribe("turtle1/pose", 1000, &monitoringTurtle);

    ros::spin();
}